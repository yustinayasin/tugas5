<?php

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware('auth:api')->get('/user', function (Request $request) {
    return $request->user();
});

Route::resource('/provinsi', ProvinsiController::class)->except(['create','edit']);
Route::resource('/kabupaten', KabupatenController::class)->except(['create','edit']);
Route::resource('/kecamatan', KecamatanController::class)->except(['create','edit']);
Route::resource('/desa', DesaController::class)->except(['create','edit']);
Route::resource('/film', FilmController::class)->except(['create','edit']);
Route::resource('/penonton', PenontonController::class)->except(['create','edit']);

Route::post('/reservasi', 'FilmController@reservasi');
Route::post('/batal-reservasi', 'FilmController@batal_reservasi');
