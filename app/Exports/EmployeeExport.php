<?php

namespace App\Exports;

use App\Models\Employee;
use Maatwebsite\Excel\Concerns\FromCollection;
use Maatwebsite\Excel\Concerns\FromView;
use Illuminate\Contracts\View\View;

class EmployeeExport implements FromView
{
    public function view(): View
    {
        return view('exports.employee', [
            'employee' => Employee::all()
        ]);
    }
}
