<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class kecamatan extends Model
{
    protected $table = 'kecamatan';
    protected $fillable = [
        'nama',
        'kabupaten_id'
    ];

    public function desa(){
        return $this->hasMany('App\Models\desa');
    }
    public function kabupaten(){
        return $this->belongsTo('App\Models\kabupaten');
    }
}
