<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class Penonton extends Model
{
    protected $table = 'penonton';
    protected $fillable = ['nama'];

    public function film(){
        return $this->belongsToMany('App\Models\Film','reservasi','penonton_id','film_id');
    }
}
