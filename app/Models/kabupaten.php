<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class kabupaten extends Model
{
    protected $table = 'kabupaten';
    protected $fillable = [
        'nama',
        'provinsi_id'
    ];

    public function kecamatan(){
        return $this->hasMany('App\Models\kecamatan');
    }
    public function provinsi(){
        return $this->belongsTo('App\Models\provinsi');
    }
}
