<?php

namespace App\Http\Controllers;

use App\Models\kabupaten;
use App\Models\kecamatan;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class KecamatanController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $kecamatan = kecamatan::all();
        return response()->json([
            'message' => 'success',
            'data' => $kecamatan
        ], Response::HTTP_OK);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required|min:2',
            'kabupaten_id'    => 'numeric'
        ]);

        try {
            if (kabupaten::where('id', $request->input('kabupaten_id'))->exists()) {
                $kecamatan = kecamatan::create($request->all());
                $response = [
                'massage' => 'Kecamatan telah ditambahkan',
                'data' => $kecamatan
            ];
                return response()->json($response, Response::HTTP_CREATED);
            } else {
                return response()->json(['message' => 'kabupaten not found'],Response::HTTP_NOT_ACCEPTABLE);
            }
        } catch (QueryException $e) {
            return response()->json([
                'message' => "Gagal | ".$e->errorInfo
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $kecamatan = kecamatan::findOrFail($id);
        $response = [
            'massage' => 'succes',
            'data' => $kecamatan
        ];
        return response()->json($response, Response::HTTP_OK);
    }



    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $kecamatan = kecamatan::findOrFail($id);
        $request->validate([
            'nama' => 'required|min:2',
            'kabupaten_id'    => 'numeric'
        ]);

        try {
            $kecamatan->update($request->all());
            $response = [
                'massage' => 'Kecamatan telah diupdate',
                'data' => $kecamatan
            ];
            return response()->json($response, Response::HTTP_OK);
        } catch (QueryException $e) {
            return response()->json([
                'message' => "Gagal | ".$e->errorInfo
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $kecamatan = kecamatan::findOrFail($id);

        try {
            $kecamatan->delete();
            $response = [
                'massage' => 'Kecamatan telah dihapus'
            ];
            return response()->json($response, Response::HTTP_OK);
        } catch (QueryException $e) {
            return response()->json([
                'message' => "Gagal | ".$e->errorInfo
            ]);
        }
    }
}
