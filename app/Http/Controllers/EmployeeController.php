<?php

namespace App\Http\Controllers;

use App\Models\Company;
use App\Models\Employee;
use App\Exports\EmployeeExport;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Http\Request;

class EmployeeController extends Controller
{
    public function export(Request $request)
    {
        if($request->get('type')=='pdf'){
            return Excel::download(new EmployeeExport, 'employee.pdf');
        }
        if($request->get('type')=='excel'){
            return Excel::download(new EmployeeExport, 'employee.xlsx');
        }
    }

    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $employee = Employee::all();
        // dd($employee->atasan()->nama);
        return view('employee.index',['employee' => $employee]);
    }
    private function _validate(Request $request){
        $validatedData = $request->validate([
            'nama' => 'required|min:2|max:50'
        ]);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $company = Company::all();
        $atasan = Employee::all();
        return view('employee.create',['company' => $company,'atasan' => $atasan]);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $all = $request->all();
        $this->_validate($request);
        if ($request->input('atasan_id')=='null') {
            unset($all['atasan_id']);
        }
        Employee::create($all);
        return redirect()->route('employee.index')->with('success','Data Berhasil ditambahkan');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $company = Company::all();
        $atasan = Employee::all();

        $employee = Employee::find($id);
    return view('employee.edit',['employee' => $employee,'company' => $company,'atasan' => $atasan]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $all = $request->all();
        $this->_validate($request);
        $employee = Employee::find($id);
        if ($all['atasan_id']=='null') {
            unset($all['atasan_id']);
        }
        $employee->update($all);
        return redirect()->route('employee.index')->with('success','Data Berhasil diubah');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $employee = Employee::find($id);
        $employee->delete();
        return redirect()->route('employee.index')->with('success','Data Berhasil dihapus');
    }
}
