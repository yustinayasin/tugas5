<?php

namespace App\Http\Controllers;

use App\Models\kabupaten;
use App\Models\provinsi;
use Illuminate\Http\Request;
use Illuminate\Database\QueryException;
use Symfony\Component\HttpFoundation\Response;

class KabupatenController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $kabupaten = kabupaten::all();
        return response()->json([
            'message' => 'success',
            'data' => $kabupaten
        ], Response::HTTP_OK);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required|min:2',
            'provinsi_id'    => 'numeric'
        ]);

        try {
            if (provinsi::where('id', $request->input('provinsi_id'))->exists()) {
                $kabupaten = kabupaten::create($request->all());
                $response = [
                'massage' => 'Kabupaten telah ditambahkan',
                'data' => $kabupaten
            ];
                return response()->json($response, Response::HTTP_CREATED);
            } else {
                return response()->json(['message' => 'provinsi not found'],Response::HTTP_NOT_ACCEPTABLE);
            }
        } catch (QueryException $e) {
            return response()->json([
                'message' => "Gagal | ".$e->errorInfo
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $kabupaten = kabupaten::findOrFail($id);
        $response = [
            'massage' => 'succes',
            'data' => $kabupaten
        ];
        return response()->json($response, Response::HTTP_OK);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $kabupaten = kabupaten::findOrFail($id);
        $request->validate([
            'nama' => 'required|min:2',
            'provinsi_id'    => 'numeric'
        ]);

        try {
            $kabupaten->update($request->all());
            $response = [
                'massage' => 'Kabupaten telah diupdate',
                'data' => $kabupaten
            ];
            return response()->json($response, Response::HTTP_OK);
        } catch (QueryException $e) {
            return response()->json([
                'message' => "Gagal | ".$e->errorInfo
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $kabupaten = kabupaten::findOrFail($id);

        try {
            $kabupaten->delete();
            $response = [
                'massage' => 'Kabupaten telah dihapus'
            ];
            return response()->json($response, Response::HTTP_OK);
        } catch (QueryException $e) {
            return response()->json([
                'message' => "Gagal | ".$e->errorInfo            ]);
        }
    }
}
