<?php

namespace App\Http\Controllers;

use App\Models\desa;
use App\Models\kecamatan;
use Illuminate\Http\Request;
use Symfony\Component\HttpFoundation\Response;

class DesaController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $desa = desa::all();
        return response()->json([
            'message' => 'success',
            'data' => $desa
        ], Response::HTTP_OK);
    }


    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $request->validate([
            'nama' => 'required|min:2',
            'kecamtan_id'    => 'numeric'
        ]);

        try {
            if (kecamatan::where('id', $request->input('kecamatan_id'))->exists()) {
                $desa = desa::create($request->all());
                $response = [
                'massage' => 'Desa telah ditambahkan',
                'data' => $desa
            ];
                return response()->json($response, Response::HTTP_CREATED);
            } else {
                return response()->json(['message' => 'Kecamatan not found'],Response::HTTP_NOT_ACCEPTABLE);
            }
        } catch (QueryException $e) {
            return response()->json([
                'message' => "Gagal | ".$e->errorInfo
            ]);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $desa = desa::findOrFail($id);
        $response = [
            'massage' => 'succes',
            'data' => $desa
        ];
        return response()->json($response, Response::HTTP_OK);
    }


    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $desa = desa::findOrFail($id);
        $request->validate([
            'nama' => 'required|min:2',
            'kabupaten_id'    => 'numeric'
        ]);

        try {
            $desa->update($request->all());
            $response = [
                'massage' => 'Desa telah diupdate',
                'data' => $desa
            ];
            return response()->json($response, Response::HTTP_OK);
        } catch (QueryException $e) {
            return response()->json([
                'message' => "Gagal | ".$e->errorInfo
            ]);
        }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $desa = desa::findOrFail($id);

        try {
            $desa->delete();
            $response = [
                'massage' => 'Desa telah dihapus'
            ];
            return response()->json($response, Response::HTTP_OK);
        } catch (QueryException $e) {
            return response()->json([
                'message' => "Gagal | ".$e->errorInfo
            ]);
        }
    }
}
