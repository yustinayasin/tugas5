<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Edit Employee</title>
    <link rel="stylesheet" href="/css/employee/edit.css">
</head>

<body>
    <form action="{{ route('employee.update',$employee->id) }}" method="POST">
        <h1>Tambah Employe</h1>
        @csrf
        @method('put')
        <label for="nama">Nama :</label>
        <input type="text" id="nama" class="form-control" name="nama" value="{{$employee->nama}}">
        <label for="atasan">Atasan :</label>
        <select name="atasan_id" class="form-control" id="atas">
            <option value="null">tidak memiliki atasan</option>
            @foreach ($atasan as $item)
            <option @if ($employee->atasan_id == $item->id)
                selected
                @endif value="{{ $item->id }}">{{ $item->nama }}</option>
            @endforeach
        </select>
        <label for="company">Company :</label>
        <select name="company_id" id="atas" class="form-control">
            @foreach ($company as $item)
            <option @if ($employee->company_id == $item->id)
                selected
                @endif value="{{ $item->id }}">{{ $item->nama }}</option>
            @endforeach
        </select>
        <div>
            <a href="{{ route('employee.index') }}" class="btn btn-back">Back</a>
            <button type="submit" class="btn btn-simpan" value="simpan">Simpan</button>
        </div>
    </form>
</body>

</html>