<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Employee</title>
    <link rel="stylesheet" href="/css/employee/index.css">
</head>

<body>
    <div class="card">
        <h3 class="m-0 p-0">List Employes</h3>
        @if ($message = Session::get('success'))
        <div class="info">
            {{$message}}
        </div>
        @endif
        <table>
            <thead>
                <tr>
                    <th>no</th>
                    <th>nama</th>
                    <th class="text-center">atasan</th>
                    <th class="text-center">company</th>
                    <th class="text-center" style="width: 200px">aksi</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($employee as $no => $item)
                <tr>
                    <td>{{ $no+1 }}</td>
                    <td>{{ $item->nama }}</td>
                    @if ($item->atasan)
                    <td>{{ $item->atasan->nama }}</td>
                    @else
                    <td>-</td>
                    @endif
                    <td>{{ $item->company->nama }}</td>
                    <td>
                        <a href="{{ route('employee.edit', $item->id) }}" class="btn btn-edit">Edit</a>
                        <form action="{{ route('employee.destroy', $item->id) }}" method="POST">
                            @csrf
                            <input type="hidden" name="_method" value="delete" />
                            <input type="submit" class="btn btn-hapus" value="Hapus">
                        </form>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
        <a href="{{ route('employee.create') }}" class="btn btn-tambah">Tambah</a>
        <a href="{{url ('/emplo/export?type=pdf')}}" class="btn btn-tambah">Export PDF</a>
        <a href="{{url ('/emplo/export?type=excel')}}" class="btn btn-tambah">Export Excel</a>
    </div>
</body>

</html>