<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Form Employee</title>
    <link rel="stylesheet" href="/css/employee/create.css">
</head>

<body>
    <form action="{{ route('employee.store') }}" method="POST">
        <h1>Tambah Employe</h1>
        @csrf
        <label for="nama">Nama :</label>
        <input type="text" id="nama" name="nama" placeholder="Masukkan nama">
        <label for="atasan">Atasan :</label>
        <select name="atasan_id" id="atas">
            <option value="null">Tidak memiliki atasan</option>
            @foreach ($atasan as $item)
            <option value="{{ $item->id }}">{{ $item->nama }}</option>
            @endforeach
        </select>
        <label for="company">Company :</label>
        <select name="company_id" id="atas">
            @foreach ($company as $item)
            <option value="{{ $item->id }}">{{ $item->nama }}</option>
            @endforeach
        </select>
        <div>
            <a href="{{ route('employee.index') }}" class="btn btn-back">Back</a>
            <button type="submit" class="btn btn-simpan" value="simpan">Submit</button>
        </div>
    </form>

    <!-- {{--  Js  --}}
<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.1.0/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-U1DAWAznBHeqEIlVSCgzq+c9gqGAJn5c/t99JyeKa9xxaYpSvHU5awsuZVVFIhvj"
        crossorigin="anonymous"></script> -->
</body>

</html>