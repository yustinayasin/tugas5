<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Tambah Company</title>
    <link rel="stylesheet" href="/css/company/create.css">
</head>

<body>
    <form action="{{ route('company.store') }}" method="POST">
        <h1>Form Company</h1>
        @csrf
        <label for="nama">Nama :</label>
        <input type="text" id="nama" class="form-control" name="nama" placeholder="Masukkan nama">
        <label for="alamat">Alamat :</label>
        <input type="text" id="alamat" class="form-control" name="alamat" placeholder="Masukkan alamat">
        <div>
            <a href="{{ route('company.index') }}" class="btn btn-back">Back</a>
            <button type="submit" class="btn btn-simpan" value="simpan">simpan</button>
        </div>
    </form>
</body>

</html>