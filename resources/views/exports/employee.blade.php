<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Document</title>
    <style>
        /* table {
            border: 1px solid black;
        }

        th,
        td {
            color: red;
            padding: 5px;
            text-align: left;
            width: 200px;
        } */
        body {
            background-color: linen;
        }

        h1 {
            color: maroon;
            margin-left: 40px;
        }
    </style>
</head>

<body>
    <table style="border: 1px solid red;text-align: center;">
        <thead>
            <tr>
                <th style="text-align:left; width: 20; height: 20;">id</th>
                <th style="text-align:left; width: 20; height: 20;">nama</th>
                <th style="text-align:left; width: 20; height: 20;">Posisi</th>
                <th style="text-align:left; width: 20; height: 20;">Perusahaan</th>
            </tr>
        </thead>
        <tbody>
            @foreach($employee as $no => $item)
            <tr>
                <td style="text-align:left; width: 20; height: 20;">{{ $no+1 }}</td>
                <td style="text-align:left; width: 20; height: 20;">{{$item->nama}}</td>
                <td style="text-align:left; width: 20; height: 20;">@if (is_null($item->atasan_id))
                    CEO
                    @elseif ($item->atasan_id == 1)
                    Direktur
                    @elseif ($item->atasan_id == 2 || $item->atasan_id == 3)
                    Manager
                    @else
                    Staff
                    @endif</td>
                <td style="text-align:left; width: 20; height: 20;">{{$item->company->nama}}</td>
            </tr>
            @endforeach
        </tbody>
    </table>
</body>

</html>